﻿using System;
using System.Threading.Tasks;

namespace MessageBrokers.Models.WorkDispatcher
{
    public abstract class Dispatcher
    {
        public void Dispatch()
        {
            var work = ListenForWork();
            var processed = ProcessWork(work);
            PublishResult(processed);
        }

        public Task DispatchAsync()
        {
            return Task.Run(() =>
            {
                while (true)
                {
                    var work = ListenForWork();
                    Task.Run(() =>
                    {
                        var processed = ProcessWork(work);
                        PublishResult(processed);
                    });
                }
            });
        }

        protected abstract object ListenForWork();
        protected abstract object ProcessWork(object work);
        protected abstract void PublishResult(object result);
    }


}
