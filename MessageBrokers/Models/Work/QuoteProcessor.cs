﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using MessageBrokers.Models.Consumers;
using MessageBrokers.Models.Producers;
using MessageBrokers.Models.Request;
using MessageBrokers.Models.Storage;
using MessageBrokers.Models.WorkResult;
using Newtonsoft.Json;

namespace MessageBrokers.Models.Work
{

    public class QuoteProcessor<T> : CompetingWorker where T : APIRequest
    {
        private readonly string _provider;
        private readonly IKeyValueStorage _storage;

        public QuoteProcessor(string provider, IKeyValueStorage storage)
        {
            _provider = provider.ToLowerInvariant();
            _storage = storage;
        }

        protected override bool CanHandleWork(object message)
        {
            if (message == null)
                return false;

            var work = (T)message;

            return work.Providers.Select(p => p.ToLowerInvariant()).Contains(_provider);
        }

        protected override object ProcessWorkload(object message)
        {
            if (message == null)
                return null;

            var work = (T)message;
            Console.WriteLine("[<-] Processing {0} {1}", _provider, work.Id);

            _storage.Increment(work.Id);
            Console.WriteLine("  [->] Increased CId " + work.Id);

            var RNG = new Random(DateTime.Now.Millisecond);
            Thread.Sleep(RNG.Next(1, 3) * 1000);

            Console.WriteLine(" [-] Processed {0} {1}", _provider, work.Id);
            return message;
        }
    }
}
