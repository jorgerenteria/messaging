﻿namespace MessageBrokers.Models.Work
{
    //http://www.enterpriseintegrationpatterns.com/patterns/messaging/BroadcastAggregate.html
    public abstract class AggregatorWorker : IWorkProcessor
    {
        protected int _timeout { get; set; }

        public AggregatorWorker(int timeoutInMilliseconds)
        {
            _timeout = timeoutInMilliseconds;
        }

        public object ProcessWork(object message)
        {
            if (message == null)
                return null;
            
            return AggregateWork(message, _timeout);
        }

        protected abstract object AggregateWork(object message, int timeout);

    }
}
