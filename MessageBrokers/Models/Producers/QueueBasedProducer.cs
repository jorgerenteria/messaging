﻿namespace MessageBrokers.Models.Producers
{
    public abstract class QueueBasedProducer : MessageProducer
    {
        protected QueueBasedProducer(string host, string username, string password)
            : base(host, username, password)
        {
        }

        public string Topic { get; set; }
        public string QueueName { get; set; }
        public bool DeclareQueue { get; set; }
        public bool PublishMessage { get; set; }

    }
}
