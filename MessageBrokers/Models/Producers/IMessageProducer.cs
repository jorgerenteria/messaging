﻿namespace MessageBrokers.Models.Producers
{
    public interface IMessageProducer
    {
        void Send<T>(T message) where T : class;
    }
}
