﻿using Amazon;
using Amazon.SimpleNotificationService.Model;
using Amazon.SimpleNotificationService;
using Amazon.Runtime;
using Newtonsoft.Json;

namespace MessageBrokers.Models.Producers
{
    public class SNSProducer : QueueBasedProducer
    {
        private readonly BasicAWSCredentials _credentials;
        private readonly RegionEndpoint _endpoint;

        public SNSProducer(string topicArn, string username, string password, RegionEndpoint endpoint)
            : base(topicArn, username, password)
        {
            _credentials = new BasicAWSCredentials(UserName, Password);
            _endpoint = endpoint;
        }

        public override void Send<T>(T message)
        {
            var snsClient = new AmazonSimpleNotificationServiceClient(_credentials, _endpoint);

            var toSend = JsonConvert.SerializeObject(message);

            snsClient.PublishAsync(new PublishRequest
            {
                Message = toSend,
                TopicArn = HostName
            }).Wait();
        }
    }
}
