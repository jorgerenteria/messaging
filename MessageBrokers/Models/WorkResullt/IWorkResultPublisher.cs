﻿using System;

namespace MessageBrokers.Models.WorkResult
{
    public interface IWorkResultPublisher
    {

        void PublishResult(object message);
    }
}
