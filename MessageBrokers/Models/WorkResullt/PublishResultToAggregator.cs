﻿using System.Collections.Generic;
using MessageBrokers.Models.Producers;
using MessageBrokers.Models.Request;

namespace MessageBrokers.Models.WorkResult
{
    public class PublishResultToAggregator<T> : IWorkResultPublisher 
        where T : APIRequest
    {
        private readonly QueueBasedProducer _publisher;

        public PublishResultToAggregator(QueueBasedProducer publisher)
        {
            _publisher = publisher;
        }

        public void PublishResult(object message)
        {
            var r = (T)message;

            PublishToCorrelationQueue(r);
        }

        private void PublishToCorrelationQueue(T message)
        {
            _publisher.QueueName = message.Id;

            _publisher.Send(message);
        }
    }
}
