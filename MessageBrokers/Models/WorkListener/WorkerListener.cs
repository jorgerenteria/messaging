﻿using MessageBrokers.Models.Consumers;
using MessageBrokers.Models.Request;

namespace MessageBrokers.Models.WorkListener
{
    public class WorkerListener : IWorkListener
    {
        private readonly IMessageConsumer _listener;

        public WorkerListener(IMessageConsumer listener)
        {
            _listener = listener;
        }

        public object ListenForWork()
        {
            return _listener.Listen();
        }
    }
}
