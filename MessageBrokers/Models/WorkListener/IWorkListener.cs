﻿namespace MessageBrokers.Models.WorkListener
{
    public interface IWorkListener
    {
        object ListenForWork();
    }
}
