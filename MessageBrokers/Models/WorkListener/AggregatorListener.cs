﻿using System;
using MessageBrokers.Models.Consumers;
using MessageBrokers.Models.Request;

namespace MessageBrokers.Models.WorkListener
{
    public class AggregatorListener : IWorkListener
    {
        private readonly IMessageConsumer _listener;

        public AggregatorListener(IMessageConsumer listener)
        {
            _listener = listener;
        }

        public object ListenForWork()
        {
            return _listener.Listen();
        }
    }
}
