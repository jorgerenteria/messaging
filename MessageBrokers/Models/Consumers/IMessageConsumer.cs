﻿using System;

namespace MessageBrokers.Models.Consumers
{
    public interface IMessageConsumer
    {
        object Listen();

    }
}
