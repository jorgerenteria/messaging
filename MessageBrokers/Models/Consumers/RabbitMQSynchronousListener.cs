﻿using System.Text;
using System.Threading;
using RabbitMQ.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections;

namespace MessageBrokers.Models.Consumers
{

    public class RabbitMQSynchronousListener<T> : RabbitMQListener<T>
    {
        public RabbitMQSynchronousListener(string host, string username, string password) 
            : base(host, username, password)
        {
        }

        protected override T GetMessage(IModel channel)
        {
            while (true)
            {
                try
                {
                    var body = channel.BasicGet(queue: QueueName, autoAck: true);
                    if (body != null)
                    {
                        var json = Encoding.UTF8.GetString(body.Body);
                        return JsonConvert.DeserializeObject<T>(json);
                    }
                    Thread.Sleep(1);
                }
                catch(Exception e)
                {
                    Console.Error.WriteLine(e);
                }
            }
        }
    }
}
