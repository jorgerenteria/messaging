﻿using StackExchange.Redis;

namespace MessageBrokers.Models.Storage
{
    public class RedisStorage : IKeyValueStorage
    {
        private static ConnectionMultiplexer redis;
        private readonly IDatabase _db;

        public RedisStorage(string endpoint, string user, string password)
        {
            redis = ConnectionMultiplexer.Connect(endpoint + ",name=" + user + ",password=" + password + ",syncTimeout=3000");
            _db = redis.GetDatabase();
        }

        public long Decrement(object key)
        {
            return _db.StringDecrement(key.ToString());
        }

        public void Delete(object key)
        {
            _db.KeyDelete(key.ToString());
        }

        public object Get(object key)
        {
            return _db.StringGet(key.ToString());
        }

        public long Increment(object key)
        {
            return _db.StringIncrement(key.ToString(),1);
        }

        public void Store(object key, object value)
        {
            _db.StringSet(key.ToString(), value.ToString());
        }

        public bool Exists(object key)
        {
            return _db.KeyExists(key.ToString());
        }
    }
}
