﻿namespace MessageBrokers.Models.Storage
{
    public interface IKeyValueStorage
    {
        void Store(object key, object value);
        object Get(object key);
        void Delete(object key);
        long Increment(object key);
        long Decrement(object key);
        bool Exists(object key);
    }
}
