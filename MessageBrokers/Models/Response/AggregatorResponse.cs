﻿using System.Collections.Generic;
using System.Linq;

namespace MessageBrokers.Models.Response
{
    public class AggregatorResponse
    {
        private IList<string> Responses;
        public int MergedResponses { get; set; }
        public int RepeatedResponses { get; set; }
        public string Id { get; set; }

        public AggregatorResponse()
        {
            Responses = new List<string>();
        }

        public void AddResponse(string response)
        {
            if (!Responses.Contains(response))
                MergedResponses++;
            else
                RepeatedResponses++;
        }
    }
}
